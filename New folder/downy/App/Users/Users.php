<?php



require_once 'User.Class.php';
require_once __DIR__.'/../Connection.php';

/** generate transcation no for API
*/


function vtimestamp(){ // this func for record to get when customer accept to do survey then use get surveyID to start a survey
	
	$response = array();
 
        // add strAnswer7
		$sql = 'INSERT INTO `Survey_Kerry_001downy` ( `vtime`, `latestStep`, `CreatedDate`, `UpdatedDate`) ';
        
        // add :answer7
		$sql.= " VALUES (:vtime, :latestStep, NOW(), NOW())";
        
        //strAnswer7=:answer7
		$sql.=" ON DUPLICATE KEY UPDATE vtime=:vtime, latestStep=:latestStep, UpdatedDate=NOW()";

   
        try {
           
		$db = getConnection();
		$stmt = $db->prepare($sql); 

        $stmt->bindParam(":latestStep",$user->latestStep);


		$stmt->execute();
		$user->id = $db->lastInsertId();
		$db = null;
        
		
		if($user->id!=0){
			$response['meta'] = array("status" => 1, "msg" => "OK");
			$response['user'] = $user;
		}else{
			
			$response["meta"] = array("status" => -1, "msg" => "Internal server error.");
		}
		 
        } catch(PDOException $e) {
            die(print_r($e));
            $response["meta"] = array("status" => -1, "msg" => "Internal server error.");
        }

	 
	return $response;
    
}


function CreateSurveyID(){ // this func for record to get when customer accept to do survey then use get surveyID to start a survey
	
	$response = array();
 
        // add strAnswer7
		$sql = 'INSERT INTO `Survey_Kerry_001downy` ( `latestStep`, `CreatedDate`, `UpdatedDate`) ';
        
        // add :answer7
		$sql.= " VALUES (:latestStep, NOW(), NOW())";
        
        //strAnswer7=:answer7
		$sql.=" ON DUPLICATE KEY UPDATE latestStep=:latestStep, UpdatedDate=NOW()";

   
        try {
           
		$db = getConnection();
		$stmt = $db->prepare($sql); 

        $stmt->bindParam(":latestStep",$user->latestStep);


		$stmt->execute();
		$user->id = $db->lastInsertId();
		$db = null;
        
		
		if($user->id!=0){
			$response['meta'] = array("status" => 1, "msg" => "OK");
			$response['user'] = $user;
		}else{
			
			$response["meta"] = array("status" => -1, "msg" => "Internal server error.");
		}
		 
        } catch(PDOException $e) {
            die(print_r($e));
            $response["meta"] = array("status" => -1, "msg" => "Internal server error.");
        }

	 
	return $response;
    
}


function checkEmail($email){

        
    $response = array();
	$user = new User();
	if ($email != null) {
        try {
		$sql = "SELECT email from `Survey_Kerry_001downy` where email='$email'";

        
			$db = getConnection();
			$stmt = $db -> prepare($sql);
			//$stmt -> bindParam("email", $email);
			$stmt -> execute();
			$result = $stmt -> fetchObject();

			$db = null;
            
            $response = $result->email;

            
		    return $response;

		} catch(PDOException $e) {

			$response["meta"] = array("status" => -1, "msg" => "Internal server error.");

		}
  
        
    }else{
        
    }
 
}

function checkEmailb($email){

        
    $response = array();
	$user = new User();
	if ($email != null) {
        try {
		$sql = "SELECT email from `Survey_Kerry_001downy` where Ismember = 'N' and email='$email'";

        
			$db = getConnection();
			$stmt = $db -> prepare($sql);
			//$stmt -> bindParam("email", $email);
			$stmt -> execute();
			$result = $stmt -> fetchObject();

			$db = null;
            
            $response = $result->email;

            
		    return $response;

		} catch(PDOException $e) {

			$response["meta"] = array("status" => -1, "msg" => "Internal server error.");

		}
  
        
    }else{
        
    }
 
}






function getUserInfo($email = null) {
    
	$response = array();
	$user = new User();
	if ($email != null) {

		$sql = "SELECT * FROM `Survey_Kerry_001downy` WHERE email='$email' ";
        

		try {
			$db = getConnection();
			$stmt = $db -> prepare($sql);
			//$stmt -> bindParam("email", $email);
			$stmt -> execute();
			$result = $stmt -> fetchObject();
			$db = null;

			if ($result != false) {

				$user -> id              = $result -> intID;
				$user -> email           = $result -> email;
                $user -> vtime           = $result -> vtime;
				$user -> refID           = $result -> refID;
				$user -> questionID      = $result -> QuestionID;
				$user -> ans1            = $result -> ans1;
				$user -> ans2            = $result -> ans2;
				$user -> ans3            = $result -> ans3;
				$user -> ans4            = $result -> ans4;
				$user -> ans5            = $result -> ans5;
				$user -> ans6            = $result -> ans6;
				$user -> ans7            = $result -> ans7;
                $user -> ans8            = $result -> ans8;
				$user -> ans9            = $result -> ans9;
				$user -> ans10           = $result -> ans10;
				$user -> ans11           = $result -> ans11;
                $user -> ans12           = $result -> ans12;
                $user -> ans13           = $result -> ans13;
                $user -> ans14           = $result -> ans14;
                $user -> ans15           = $result -> ans15;
                $user -> ans16           = $result -> ans16;
                $user -> ans17           = $result -> ans17;
                $user -> ans18           = $result -> ans18;
                $user -> ans19           = $result -> ans19;
                $user -> ans20           = $result -> ans20;
                $user -> updateddate     = $result -> UpdatedDate;
                $user -> latestStep      = $result -> latestStep;
                $user -> IsMember        = $result -> isMember;
                $user -> PotentialEarn   = $result -> PotentialEarn;
                $user -> pointsGiven     = $result -> intPointsGiven;

				$response['meta'] = array("status" => 1, "msg" => "OK");
				$response['user'] = $user;
                



			} else {

				$response["meta"] = array("status" => -1, "msg" => "No user for this email.");
			}
            

			

		} catch(PDOException $e) {

			$response["meta"] = array("status" => -1, "msg" => "Internal server error.");
		}
	} else {
		$response["meta"] = array("status" => 0, "msg" => "Missing data.");
	}
	return $response;
}





/* Retrieve all users who finish step 4
 * response status code 
 *  1-Ok,-1=Errors, 0= Missing data
 *
 */
function getID_vtime($vtime) { // Lucky Draw Function ???

	$response = array();
	$user = new User();
	

		//$sql = "SELECT count(*) as Total FROM citibank_users WHERE intLatestStep=4";
            $sql = "SELECT intID  FROM `Survey_Kerry_001downy` where vtime = '$vtime' ";
		try {
			$db = getConnection();
			$stmt = $db -> prepare($sql);
			$stmt -> execute();
			$result = $stmt -> fetchObject();
			$db = null;

			if ($result != false) {

				$response['meta'] = array("status" => 1, "msg" => "OK");
				$response['latestID'] = $result->intID;



			} else {

				$response["meta"] = array("status" => -1, "msg" => "No user for this email.");
			}

			

		} catch(PDOException $e) {

			$response["meta"] = array("status" => -1, "msg" => "Internal server error.");
		}

	return $response;
    
}

function getLatestSurveyID() { // Lucky Draw Function ???

	$response = array();
	$user = new User();
	

		//$sql = "SELECT count(*) as Total FROM citibank_users WHERE intLatestStep=4";
            $sql = "SELECT max(intID) as `intID` FROM `Survey_Kerry_001downy`";
		try {
			$db = getConnection();
			$stmt = $db -> prepare($sql);
			$stmt -> execute();
			$result = $stmt -> fetchObject();
			$db = null;

			if ($result != false) {

				$response['meta'] = array("status" => 1, "msg" => "OK");
				$response['latestID'] = $result->intID;



			} else {

				$response["meta"] = array("status" => -1, "msg" => "No user for this email.");
			}

			

		} catch(PDOException $e) {

			$response["meta"] = array("status" => -1, "msg" => "Internal server error.");
		}

	return $response;
    
}

/* Retrieve user with email
 * response status code 
 *  1-Ok,-1=Errors, 0= Missing data
 *
 */

function getUser($surveyID = null) {
    
	$response = array();
	$user = new User();
	if ($surveyID != null) {

		$sql = "SELECT * FROM `Survey_Kerry_001downy` WHERE intID=$surveyID";
        

		try {
			$db = getConnection();
			$stmt = $db -> prepare($sql);
			//$stmt -> bindParam("email", $email);
			$stmt -> execute();
			$result = $stmt -> fetchObject();
			$db = null;

			if ($result != false) {

				$user -> id              = $result -> intID;
				$user -> email           = $result -> email;
                $user -> vtime           = $result -> vtime;
				$user -> refID           = $result -> refID;
				$user -> questionID      = $result -> QuestionID;
				$user -> ans1            = $result -> ans1;
				$user -> ans2            = $result -> ans2;
				$user -> ans3            = $result -> ans3;
				$user -> ans4            = $result -> ans4;
				$user -> ans5            = $result -> ans5;
				$user -> ans6            = $result -> ans6;
				$user -> ans7            = $result -> ans7;
                $user -> ans8            = $result -> ans8;
				$user -> ans9            = $result -> ans9;
				$user -> ans10           = $result -> ans10;
				$user -> ans11           = $result -> ans11;
                $user -> ans12           = $result -> ans12;
                $user -> ans13           = $result -> ans13;
                $user -> ans14           = $result -> ans14;
                $user -> ans15           = $result -> ans15;
                $user -> ans16           = $result -> ans16;
                $user -> ans17           = $result -> ans17;
                $user -> ans18           = $result -> ans18;
                $user -> ans19           = $result -> ans19;
                $user -> ans20           = $result -> ans20;
                $user -> updateddate     = $result -> UpdatedDate;
                $user -> latestStep      = $result -> latestStep;
                $user -> IsMember        = $result -> isMember;
                $user -> PotentialEarn   = $result -> PotentialEarn;
                $user -> pointsGiven     = $result -> intPointsGiven;

				$response['meta'] = array("status" => 1, "msg" => "OK");
				$response['user'] = $user;
                



			} else {

				$response["meta"] = array("status" => -1, "msg" => "No user for this email.");
			}
            

			

		} catch(PDOException $e) {

			$response["meta"] = array("status" => -1, "msg" => "Internal server error.");
		}
	} else {
		$response["meta"] = array("status" => 0, "msg" => "Missing data.");
	}
	return $response;
}

/*
 * Save user data
 * response status code 
 *  1-Ok,-1=Errors, 0= Missing data 
 */
 
function saveUser($user){
	
	$response = array(); 


        
        // add strAnswer7
		$sql = 'INSERT INTO `Survey_Kerry_001downy` ( `intID`,`vtime`, `email`, `refID`, `QuestionID`, `ans1`, `ans2`, `ans3`, `ans4`, `ans5`, `ans6`,`ans7`, `ans8`, `ans9`, `ans10`, `ans11`, `ans12`, `ans13`, `ans14` ,`ans15`, `ans16`,`ans17`, `ans18`, `ans19`, `ans20`,`intPointsGiven` , `UpdatedDate`,`latestStep`,`IsMember`,`PotentialEarn`,`PotentialEarnDate`) ';
        
        // add :answer7
		$sql.= " VALUES ( :id, :vtime, :email, :refID, :questionID, :ans1, :ans2, :ans3, :ans4, :ans5, :ans6, :ans7 , :ans8, :ans9, :ans10, :ans11, :ans12, :ans13, :ans14 ,:ans15, :ans16, :ans17, :ans18, :ans19, :ans20,:pointsGiven, NOW(),  :latestStep, :IsMember,:PotentialEarn, :PotentialEarnDate)";
        
        //strAnswer7=:answer7
		$sql.=" ON DUPLICATE KEY UPDATE  intID =:id, vtime =:vtime, email =:email, refID =:refID, QuestionID =:questionID, ans1 =:ans1, ans2 =:ans2, ans3 =:ans3, ans4 =:ans4, ans5 =:ans5, ans6 =:ans6, ans7 =:ans7 , ans8 =:ans8, ans9 =:ans9, ans10 =:ans10, ans11 =:ans11, ans12=:ans12, ans13 =:ans13, ans14 =:ans14, ans15 =:ans15, ans16 =:ans16, ans17 =:ans17, ans18 =:ans18, ans19 =:ans19, ans20 =:ans20, intPointsGiven=:pointsGiven, UpdatedDate=NOW(), latestStep=:latestStep, IsMember=:IsMember, PotentialEarn=:PotentialEarn, PotentialEarnDate=:PotentialEarnDate ";
         
   
        
        try {
           
		$db = getConnection();
		$stmt = $db->prepare($sql); 

        $stmt->bindParam(":id", $user->id);
        $stmt->bindParam(":vtime", $user->vtime);
		$stmt->bindParam(":email", $user->email);
		$stmt->bindParam(":refID", $user->refID);
        $stmt->bindParam(":questionID",$user->questionID);
		$stmt->bindParam(":ans1",$user->ans1);
        $stmt->bindParam(":ans2",$user->ans2);
        $stmt->bindParam(":ans3",$user->ans3);
        $stmt->bindParam(":ans4",$user->ans4);
        $stmt->bindParam(":ans5",$user->ans5);
        $stmt->bindParam(":ans6",$user->ans6);
        $stmt->bindParam(":ans7",$user->ans7);
        $stmt->bindParam(":ans8",$user->ans8);
        $stmt->bindParam(":ans9",$user->ans9);
        $stmt->bindParam(":ans10",$user->ans10);
        $stmt->bindParam(":ans11",$user->ans11);
        $stmt->bindParam(":ans12",$user->ans12);
        $stmt->bindParam(":ans13",$user->ans13);
        $stmt->bindParam(":ans14",$user->ans14);
        $stmt->bindParam(":ans15",$user->ans15);
        $stmt->bindParam(":ans16",$user->ans16);
        $stmt->bindParam(":ans17",$user->ans17);
        $stmt->bindParam(":ans18",$user->ans18);
        $stmt->bindParam(":ans19",$user->ans19);
        $stmt->bindParam(":ans20",$user->ans20);
        $stmt->bindParam(":pointsGiven",$user->pointsGiven);
        $stmt->bindParam(":latestStep",$user->latestStep );
        $stmt->bindParam(":IsMember",$user->IsMember );
        $stmt->bindParam(":PotentialEarn",$user->PotentialEarn );
        $stmt->bindParam(":PotentialEarnDate",$user->PotentialEarnDate );


		$stmt->execute();
		$user->id = $db->lastInsertId();
		$db = null;
        
 
		
		if($user->id!=0){
			$response['meta'] = array("status" => 1, "msg" => "OK");
			$response['user'] = $user;
		}else{
			
			$response["meta"] = array("status" => -1, "msg" => "Internal server error.");
		}
		 
	} catch(PDOException $e) {
		die(print_r($e));
		$response["meta"] = array("status" => -1, "msg" => "Internal server error.");
	}

	 
	return $response;
    
}


?>