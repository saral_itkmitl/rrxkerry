<?php

require_once "App/Users/Users.php";
/* s*/

$msgErr="";
$user=new User();

if ($_SERVER["REQUEST_METHOD"] == "POST") {

        
        $email =( isset ($_POST["email"]) && trim ($_POST["email"]) != '' ) ? trim ($_POST["email"]) : '';
        
        $surveyID =( isset ($_POST["surveyID"]) && trim ($_POST["surveyID"]) != '' ) ? trim ($_POST["surveyID"]) : '';
		$answer1 =( isset ($_POST["answer1"]) && trim ($_POST["answer1"]) != '' ) ? trim ($_POST["answer1"]) : '';
		$answer2 =( isset ($_POST["answer2"]) && trim ($_POST["answer2"]) != '' ) ? trim ($_POST["answer2"]) : '';
		$answer3 =( isset ($_POST["answer3"]) && trim ($_POST["answer3"]) != '' ) ? trim ($_POST["answer3"]) : '';
		$answer4 =( isset ($_POST["answer4"]) && trim ($_POST["answer4"]) != '' ) ? trim ($_POST["answer4"]) : '';
		$answer5 =( isset ($_POST["answer5"]) && trim ($_POST["answer5"]) != '' ) ? trim ($_POST["answer5"]) : '';
        $answer6 =( isset ($_POST["answer6"]) && trim ($_POST["answer6"]) != '' ) ? trim ($_POST["answer6"]) : '';
        
        
       
        if($email == '0'){
            if($surveyID!="" && $answer1!="" && $answer2!="" && $answer3!="" && $answer4!="" && $answer5!="" && $answer6!="") {
        if (!filter_var($surveyID, FILTER_SANITIZE_STRING)) {
			$msgErr = "Invalid surveryID"; 

		}
		if (!filter_var($answer1, FILTER_SANITIZE_STRING)) {
			$msgErr = "Invalid answer."; 

		}
		if (!filter_var($answer2, FILTER_SANITIZE_STRING)) {
			$msgErr = "Invalid answer."; 

		}
		if (!filter_var($answer3, FILTER_SANITIZE_STRING)) {
			$msgErr = "Invalid answer."; 

		}
		if (!filter_var($answer4, FILTER_SANITIZE_STRING)) {
			$msgErr = "Invalid answer."; 

		}
		if (!filter_var($answer5, FILTER_SANITIZE_STRING)) {
			$msgErr = "Invalid answer."; 

		}
        if (!filter_var($answer6, FILTER_SANITIZE_STRING)) {
			$msgErr = "Invalid answer."; 

		}
        
            
            
            $res =getUser($surveyID);
                    
                    // recheck data can get from DB
                    if($res["meta"]["status"]==1){
				        $user=$res["user"];
                        //echo"<br>Test :".$user->ans6;
                    }else{
                        //if not will show this
                        $msgErr=$res["meta"]["msg"];
                    }
            
        // if not null asign to variable
            
            $user->email  =$email;    
            $user->id  =$surveyID;
            $user->ans1=$answer1;
            $user->ans2=$answer2;
            $user->ans3=$answer3;
            $user->ans4=$answer4;
            $user->ans5=$answer5;
            $user->ans6=$answer6;
            
            $user->latestStep=2; // process status
            
         
            $result=saveUser($user);
            
            if($result["meta"]["status"]!=1){
  					$msgErr=$result["meta"]["msg"];
                    
  				}
  				else{
                    //get latest servey ID to start do survey select by max record
                    $res =getLatestSurveyID();
                    $surveyID=$res["latestID"];

                    $user->id = $surveyID;
                    
  				}

                }else{

                $msgErr="Ops! something wrong! Please again later.";
                }
        }else{
            
            $res =getUser($surveyID); 
            if($res["meta"]["status"]==1){
				        $user=$res["user"];
                        
                    }else{
                        
                        $msgErr=$res["meta"]["msg"];
                    }
                    $user->email=0;
                    $user->id=$surveyID;
                     
                    $result=saveUser($user);
                        if($result["meta"]["status"]!=1){
                        $msgErr=$result["meta"]["msg"];
                        }else{
                        $user=$result["user"];
                        }
        }
    

}


?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Rabbit Rewards Survey</title>
<meta property="og:image" content="https://rewards.rabbit.co.th/images/favicon/rabbit/mstile-144x144.png"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>   
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>



  <!-- Mobile Specific Metas –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="https://fonts.googleapis.com/css?family=Kanit:300,300i,400,400i,500&amp;subset=thai" rel="stylesheet">

  <!-- CSS –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/survey-custom.css">
  
  
  
 


 <!-- Google Tag Manager : rabbitrewards.co.th-->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PTXX5FT');</script>
<!-- End Google Tag Manager -->

</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PTXX5FT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- MAIN HEADER-->
<div class="main-header">
    <div class="head-left">
        <img src="images/RR_tagline.png"  onerror="this.onerror=null; this.src='images/RR_tagline.svg'" style="height: 43px;">
    </div>
    <div class="head-right">
        <img src="images/rrlogo.png" onerror="this.onerror=null; this.src='images/rrlogo.svg'" style="height: 40px;">
    </div>  
    <div style="clear:both;"></div> 
</div> 
<!-- HEAD FUNCTION -->

            <form action="verifyAccount.php" method="post" id="optionForm" class="form-group"> 
                <input type="hidden" value="<?php echo $surveyID; ?>" name="surveyID" />  

                    
                <div class="container">
                  <div class="row">
                      <div class="three columns" style="height:1px;">&nbsp;</div>
                      <div class="six columns" style="text-align:center; padding-top:15px;">
                         <span style="font-size:1.2em; color:white;" >
                            ขอบคุณที่ร่วมตอบแบบสอบถาม<br>
                            <span style="color:#000; font-weight:500; font-size:1.4em;">กรุณากรอกข้อมูลเพื่อรับ</span>
                         </span>

                         <img src="images/survey/icn-100p2.png" style="max-height:90px;">
                      </div>
                      <div class="three columns" style="height:1px;">&nbsp;</div>
                  </div>

                    <div class="row">
                        <div class="three columns" style="height:1px;">&nbsp;</div>
                        <div class="six columns"style="padding-left:10px; padding-right:10px;"><br>
                            <input type="email" name="email" class="u-full-width" placeholder="อีเมล" title="กรุณากรอกอีเมล์ให้ถูกต้อง"  required />
                        </div>
                        <div class="three columns" style="height:1px;">&nbsp;</div>
                    </div>
                    
                    
                    <div class="row">
                        <div class="twelve columns" style="text-align:center; margin-top:0; padding-top:0px;">
             
                            
                            <button type="submit" name="btnSave" class="button-primary" >ยืนยัน</button><br>
                        <img src="images/survey/bg-getpoint.jpg" style="max-height:320px; box-sizing:border-box;" >
                            <span style="font-size:small; color:white;"><br>
                              * หากคุณเป็นสมาชิก Rabbit Rewards อยู่แล้ว<br>กรุณากรอกอีเมล์ที่ผูกกับบัญชี Rabbit Rewards
                            </span>
                        </div>
                    </div>
                </div>
            </form>




</body>

</html>


