<?php

require_once "App/Users/Users.php";


$msgErr="";


if ($_SERVER["REQUEST_METHOD"] == "POST") {



        $surveyID =( isset ($_POST["surveyID"]) && trim ($_POST["surveyID"]) != '' ) ? trim ($_POST["surveyID"]) : '';
		$email =( isset ($_POST["email"]) && trim ($_POST["email"]) != '' ) ? trim ($_POST["email"]) : '';
        

    
		if($email!="" && $surveyID!="") {
		if (!filter_var($email, FILTER_SANITIZE_STRING)) {
			$msgErr = "Invalid answer."; 

		}
		if (!filter_var($surveyID, FILTER_SANITIZE_STRING)) {
			$msgErr = "Invalid answer."; 

		}
        

        
            

	}else{
	
	$msgErr="Ops! something wrong! Please again later.";
	}
    
    
    
    
}


?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Rabbit Rewards Survey</title>
<meta property="og:image" content="https://rewards.rabbit.co.th/images/favicon/rabbit/mstile-144x144.png"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>   
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>



  <!-- Mobile Specific Metas –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="https://fonts.googleapis.com/css?family=Kanit:300,300i,400,400i,500&amp;subset=thai" rel="stylesheet">

  <!-- CSS –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/survey-custom.css">
  



 <!-- Google Tag Manager : rabbitrewards.co.th-->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PTXX5FT');</script>
<!-- End Google Tag Manager -->
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PTXX5FT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- MAIN HEADER -->
<div class="main-header">
    <div class="head-left">
        <img src="images/RR_tagline.png"  onerror="this.onerror=null; this.src='images/RR_tagline.svg'" style="height: 43px;">
    </div>
    <div class="head-right">
        <img src="images/rrlogo.png" onerror="this.onerror=null; this.src='images/rrlogo.svg'" style="height: 40px;">
    </div>  
    <div style="clear:both;"></div> 
</div> 
<!-- HEAD FUNCTION -->


<?php

        $reqEmail = checkEmail($email);
        
        
        if(is_null($reqEmail)){
            
            $CheckResult = ValidateEmail($email);
            if($CheckResult==0){
                
                    $user=new User();
                    $res =getUser($surveyID);
                
                    
                    if($res["meta"]["status"]==1){
				        $user=$res["user"];
                        
						if($user->pointsGiven=='100'){
							
						}else{
							$user->email=$email;
							
						}
						
                    }else{
                        
                        $msgErr=$res["meta"]["msg"];
                    }
                
                    
					
					
                    $user->id=$surveyID;
                    $user->IsMember =  'Y';
                
                    $result=saveUser($user);
                        if($result["meta"]["status"]!=1){
                            $msgErr=$result["meta"]["msg"];
                        }else{
                            $user=$result["user"];
                        }
                    
                    $user=new User();
                    $res =getUser($surveyID);
                
                    
                    if($res["meta"]["status"]==1){
				        $user=$res["user"];

                        
                        
                    }else{
                        
                        $msgErr=$res["meta"]["msg"];
                    }
                
                
					//echo"<br>before set pointgiven = 0<br>";
                   if($user->pointsGiven==0 ){
                       //echo"<br>start to earn point<br>";
                       $APIres=sendRewardPoints($email,$xmlString); 
					   //echo"<br>response code".$APIres;
                       if($APIres->response_code==0){
                           $user->pointsGiven=100;
                           $user->IsMember =  'Y';
                           $user->latestStep=3;
                            // SAVE IN DB   
                           $result=saveUser($user);
                                if($result["meta"]["status"]!=1){
                                $msgErr=$result["meta"]["msg"];
                                }else{
                                $user=$result["user"];
                                }
                           ?>

<div class="container">
    <div class="row">
        <div class="three columns" style="height:1px;">&nbsp;</div>
        <div class="six columns" style="text-align:center; padding-top:15px;">
            <span style="font-size:1.2em; color:white;" >ยินดีด้วยค่ะ</span><br>
            <img src="images/survey/icn-100p2.png" style="max-height:90px;"><br>
            <span style="font-size:1.6em;" >ได้ถูกเพิ่มบนบัญชีของคุณแล้ว</span><br><br><br>
            <button class="button-primary" onclick="window.location='https://bit.ly/2HOOWof'">ดูบัญชี</button><br><br>
            <button class="button-primary" onclick="window.location='https://bit.ly/2KBClT1'">ดูรางวัล</button><br>
            <img src="images/survey/bg-ptai-check-ac.jpg" style="max-height:320px;"  class="u-max-full-width" ><br>
            <span style="font-size:x-small; color:#83b200;">WP01</span>
        </div>
        <div class="three columns" style="height:1px;">&nbsp;</div>
    </div>
</div>
                           
                           <?php
                           
                       }else{
                           $user->pointsGiven='0';
                           $user->IsMember =  'Y';
                             
                           $result=saveUser($user);
                                if($result["meta"]["status"]!=1){
                                $msgErr=$result["meta"]["msg"];
                                }else{
                                $user=$result["user"];
                                }
                           ?>

						<div class="container">
							<div class="row">
								<div class="three columns" style="height:1px;">&nbsp;</div>
								<div class="six columns" style="text-align:center;">
									<br>

									<span style="font-size:1.6em; color:white;" >โอ๊ะโอ! ดูเหมือนว่ามีบางอย่างไม่ถูกต้อง</span><br>
									<span style="font-size:1.2em; color:white;" >กรุณาลองใหม่อีกครั้ง หรือติดต่อ call center ของเราที่ <a href="tel:026183777" style="color:white;">02-618-3777</a></span><br>
									<span style="font-size:x-small; color:#83b200;">WP03</span>

								</div>
								<div class="three columns" style="height:1px;">&nbsp;</div>
							</div>
						</div>
                           <?php
                       }
                       
                       
                   }else{
                     
                       ?>

<div class="container">
    <div class="row">
        <div class="three columns" style="height:1px;">&nbsp;</div>
        <div class="six columns" style="text-align:center; padding-top: 15px;">
            <span style="font-size:1.2em; color:white;" >ยินดีด้วยค่ะ</span><br>
            <img src="images/survey/icn-100p2.png" style="max-height:90px;"><br>
            <span style="font-size:1.6em;" >ได้ถูกเพิ่มบนบัญชีของคุณแล้ว</span><br><br><br>
            <button class="button-primary" onclick="window.location='https://bit.ly/2HOOWof'">ดูบัญชี</button><br><br>
            <button class="button-primary" onclick="window.location='https://bit.ly/2KBClT1'">ดูรางวัล</button><br>
            <img src="images/survey/bg-ptai-check-ac.jpg" style="max-height:320px;"  class="u-max-full-width" >
            <br><span style="font-size:x-small; color:#83b200;">WP04</span>
        </div>
        <div class="three columns" style="height:1px;">&nbsp;</div>
    </div>
</div>
                       
                       <?php
                   }
                
                
            }else{
                        ?>
        <div class="container">
<div class="row">
        <div class="three columns" style="height:1px;">&nbsp;</div>
        <div class="six columns" style="text-align:center; padding-top: 15px;">
            <span style="font-size:1.2em; color:white;" >ยินดีด้วยค่ะ</span><br>
            <img src="images/survey/icn-100p2.png" style="max-height:90px;"><br>
            <span style="font-size:1.6em;" >ได้ถูกเพิ่มบนบัญชีของคุณแล้ว</span><br><br><br>
            <button class="button-primary" onclick="window.location='https://bit.ly/2HOOWof'">ดูบัญชี</button><br><br>
            <button class="button-primary" onclick="window.location='https://bit.ly/2KBClT1'">ดูรางวัล</button><br>
            <img src="images/survey/bg-ptai-check-ac.jpg" style="max-height:320px;"  class="u-max-full-width" >
            <br><span style="font-size:x-small; color:#83b200;">WP05</span>
            
        </div>
        <div class="three columns" style="height:1px;">&nbsp;</div>
    </div>
</div>
        
        <?php 
            }
            
        }else{
          
        ?>
        <div class="container">
<div class="row">
        <div class="three columns" style="height:1px;">&nbsp;</div>
        <div class="six columns" style="text-align:center; padding-top: 15px;">
            <span style="font-size:1.2em; color:white;" >ยินดีด้วยค่ะ</span><br>
            <img src="images/survey/icn-100p2.png" style="max-height:90px;"><br>
            <span style="font-size:1.6em;" >ได้ถูกเพิ่มบนบัญชีของคุณแล้ว</span><br><br><br><br>
            <button class="button-primary" onclick="window.location='https://bit.ly/2HOOWof'">ดูบัญชี</button><br><br>
            <button class="button-primary" onclick="window.location='https://bit.ly/2KBClT1'">ดูรางวัล</button><br>
            <img src="images/survey/bg-ptai-check-ac.jpg" style="max-height:320px;"  class="u-max-full-width" >
            <br><span style="font-size:x-small; color:#83b200;">WP02</span>
            
        </div>
        <div class="three columns" style="height:1px;">&nbsp;</div>
    </div>
</div>
        
        <?php    
        }
        
?>


</body>

</html>


