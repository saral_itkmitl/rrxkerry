<?php

require_once "App/Users/Users.php";

$user=new User();



date_default_timezone_set("Asia/Bangkok");
$dt = new DateTime();

$vtime= $dt->format('Y-m-d H:i:s:u');

$user->vtime =$vtime;
$user->latestStep=1;
$email = '0';


$result=saveUser($user);
            
    if($result["meta"]["status"]!=1){
  		$msgErr=$result["meta"]["msg"];

  	}else{

        $res = getID_vtime($vtime);
        $surveyID=$res["latestID"];

        

  		}


?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Rabbit Rewards x Downy</title>
<meta property="og:image" content="https://rewards.rabbit.co.th/images/favicon/rabbit/mstile-144x144.png"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>   
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript" src="js/jquery.validationEngine.js"></script>

  <!-- Mobile Specific Metas –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="https://fonts.googleapis.com/css?family=Kanit:300,300i,400,400i,500&amp;subset=thai" rel="stylesheet">

  <!-- CSS –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/survey-custom.css">
  <link rel="stylesheet" href="css/validationEngine.jquery.css">
  
  
  



 <!-- Google Tag Manager : rabbitrewards.co.th-->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PTXX5FT');</script>
<!-- End Google Tag Manager -->
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PTXX5FT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- MAIN HEADER-->
<div class="main-header">
    <div class="head-left">
        <img src="images/RR_tagline.png"  onerror="this.onerror=null; this.src='images/RR_tagline.svg'" style="height: 43px;">
    </div>
    <div class="head-right">
        <img src="images/rrlogo.png" onerror="this.onerror=null; this.src='images/rrlogo.svg'" style="height: 40px;">
    </div>  
    <div style="clear:both;"></div> 
</div>



<div class="container" style="margin-bottom:0; padding-bottom:0;">
   <form action="result.php" method="post" id="optionForm" class="form-group">
   
    <input type="hidden" name="surveyID" value="<?php echo $surveyID; ?>"/>
    <input type="hidden" name="email" value="<?php echo $email; ?>"/>
    <br/>
    <div class="row">
        <div class="twelve columns" style="text-align:center;">
            <span style="color:#fff; font-size:2.2em; font-weight:600;">รับ <img src="images/survey/icn-100p.png" style="max-height:100px;"></span><br>
            <span style="color:#fff; font-size:1.6em;">เพียงตอบคำถามด้านล่าง</span><br><br>
            <ol >
     
                <li class="box-q">คุณใช้ผลิตภัณฑ์น้ำยาปรับผ้านุ่มในการทำความสะอาดเสื้อผ้าหรือไม่<br/>
                        <input type="radio" name="answer1"  value="ใช้" id="q1-a" required class="validate[required]" >
                            <label class="label-body" for="q1-a">ใช้</label><br>
                        <input type="radio" name="answer1" value="ไม่ใช้" id="q1-b"> 
                            <label class="label-body" for="q1-b">ไม่ใช้</label><br>

                </li>
                
               
                <li class="box-q">จากที่ได้ทดลองใช้ผลิตภัณฑ์ Downy Beads คุณให้คะแนนความเข้มข้นของกลิ่น ระดับที่เท่าไหร่จาก 1-5<br/>
                    <span style="color:#8dc63f; font-size:small;">(1 คือ น้อยที่สุด และ 5 คือมากที่สุด ตามลำดับ)</span><br>
                        <input type="radio" name="answer2"  value="1" id="q2-a" required class="validate[required]" title="กรุณาตอบแบบสอบถามให้ครบถ้วนค่ะ" >
                            <label class="label-body" for="q2-a">1</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="answer2" value="2" id="q2-b"> 
                            <label class="label-body" for="q2-b">2</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="answer2" value="3" id="q2-c">
                            <label class="label-body" for="q2-c">3</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="answer2" value="4" id="q2-d">
                            <label class="label-body" for="q2-d">4</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="answer2" value="5" id="q2-e">
                            <label class="label-body" for="q2-e">5</label>

                </li>
                
               
                <li class="box-q">จากที่ได้ทดลองใช้ผลิตภัณฑ์ Downy Beads คุณให้คะแนนความหอมของผลิตภัณฑ์ ระดับที่เท่าไหร่จาก 1-5<br/>
                       <span style="color:#8dc63f; font-size:small;">(1 คือ น้อยที่สุด และ 5 คือมากที่สุด ตามลำดับ)</span><br>
                        <input type="radio" name="answer3" value="1" id="q3-a" required class="validate[required]" title="กรุณาตอบแบบสอบถามให้ครบถ้วนค่ะ" >
                            <label class="label-body" for="q3-a">1</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="answer3" value="2" id="q3-b"> 
                            <label class="label-body" for="q3-b">2</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="answer3" value="3" id="q3-c">
                            <label class="label-body" for="q3-c">3</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="answer3" value="4" id="q3-d">
                            <label class="label-body" for="q3-d">4</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="answer3" value="5" id="q3-e">
                            <label class="label-body" for="q3-e">5</label>

 
                </li>
                
                
                <li class="box-q">จากที่ได้ทดลองใช้ผลิตภัณฑ์ Downy Beads คุณให้คะแนนความพึงพอใจในความหอมติดทนนาน ระดับที่เท่าไหร่จาก 1-5<br/>
                       <span style="color:#8dc63f; font-size:small;">(1 คือ น้อยที่สุด และ 5 คือมากที่สุด ตามลำดับ)</span><br>
                        <input type="radio" name="answer4" value="1" id="q4-a" required class="validate[required]" title="กรุณาตอบแบบสอบถามให้ครบถ้วนค่ะ" >
                            <label class="label-body" for="q4-a">1</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="answer4" value="2" id="q4-b"> 
                            <label class="label-body" for="q4-b">2</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="answer4" value="3" id="q4-c">
                            <label class="label-body" for="q4-c">3</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="answer4" value="4" id="q4-d">
                            <label class="label-body" for="q4-d">4</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="answer4" value="5" id="q4-e">
                            <label class="label-body" for="q4-e">5</label>

                </li>
                
            
                <li class="box-q">หลังจากที่ทดลองใช้คุณจะหาซื้อผลิตภัณฑ์ Downy Beads มาใช้อีกหรือไม่<br/>
                    <input type="radio" name="answer5" value="กลับมาใช้อีกแน่นอน" id="q5-a" required class="validate[required]" title="กรุณาตอบแบบสอบถามให้ครบถ้วนค่ะ" >
                        <label class="label-body" for="q5-a">กลับมาใช้อีกแน่นอน</label><br>
                    <input type="radio" name="answer5" value="อาจจะกลับมาใช้" id="q5-b">
                        <label class="label-body" for="q5-b">อาจจะกลับมาใช้</label><br>
                    <input type="radio" name="answer5" value="ไม่ใช้" id="q5-c">
                        <label class="label-body" for="q5-c">ไม่ใช้</label><br>

 
                </li>
             
                <li class="box-q">คุณจะแนะนำให้สมาชิกครอบครัวหรือเพื่อนมาใช้ผลิตภัณฑ์ Downy Beads หรือไม่<br/>
                    <input type="radio" name="answer6" value="แนะนำ" id="q6-a" required class="validate[required]" title="กรุณาตอบแบบสอบถามให้ครบถ้วนค่ะ" >
                        <label class="label-body" for="q6-a">แนะนำ</label><br>
                    <input type="radio" name="answer6" value="อาจจะ" id="q6-b">
                        <label class="label-body" for="q6-b">อาจจะ</label><br>
                    <input type="radio" name="answer6" value="ไม่แนะนำ" id="q6-c">
                        <label class="label-body" for="q6-c">ไม่แนะนำ</label><br>

 
                </li>
                
            </ol>
                
        </div>
    </div>
    

    <div class="row">
        <div class="three columns" style="height:1px;">&nbsp;</div>
        <div class="six columns" style="text-align:center; margin-botton:0px; padding-bottom:0px;">
        <button type="submit" name"btnSave" class="button-primary" >ส่งข้อมูล</button><br>
        <img src="images/survey/bg-survey.jpg" class="u-max-full-width" style="max-height:400px;" /><br>
        </div>
        <div class="three columns" style="height:0px;">&nbsp;</div>
    </div>
    </form>
</div>





</body>

</html>