<?php

require_once "App/Users/Users.php";



$emailErr="";

$email =( isset ($_GET["email"]) && trim ($_GET["email"]) != '' ) ? trim ($_GET["email"]) : '';

if($email!="") {
    
    //detect error email
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$msgErr = "Invalid email format"; 

	}


}else{
   $msgErr="No email";
}


/*
STEP:
1. get email from get parameter
2. Show result
    2.1 Found: add point, save and show result.
	2.2 Not found: show not found the page
	2.3 Found but already get point:  show not found the page
	2.4 API cannot earn Point: 
*/
    
	



?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Rabbit Rewards Survey</title>
<meta property="og:image" content="https://rewards.rabbit.co.th/images/favicon/rabbit/mstile-144x144.png"/>
<script>
  
</script>
  <!-- Mobile Specific Metas –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="https://fonts.googleapis.com/css?family=Kanit:300,300i,400,400i,500&amp;subset=thai" rel="stylesheet">

  <!-- CSS –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/survey-custom.css">




 <!-- Google Tag Manager : rabbitrewards.co.th-->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PTXX5FT');</script>
<!-- End Google Tag Manager -->
</head>

<body >
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PTXX5FT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- MAIN HEADER-->
<div class="main-header">
    <div class="head-left">
        <img src="images/RR_tagline.png"  onerror="this.onerror=null; this.src='images/RR_tagline.svg'" style="height: 43px;">
    </div>
    <div class="head-right">
        <img src="images/rrlogo.png" onerror="this.onerror=null; this.src='images/rrlogo.svg'" style="height: 40px;">
    </div>  
    <div style="clear:both;"></div> 
</div>
<?php 
    $reqEmail = checkEmailb($email);

    
    if(is_null($reqEmail)){
        
        ?>
		<div class="container">
        <div class="row">
        <div class="three columns" style="height:1px;">&nbsp;</div>
        <div class="six columns" style="text-align:center; padding-top:15px;">
            <span style="font-size:1.2em; color:white;" >ยินดีด้วยค่ะ</span><br>
            <img src="images/survey/icn-100p2.png" style="max-height:90px;"><br>
            <span style="font-size:1.6em;" >ได้ถูกเพิ่มบนบัญชีของคุณแล้ว</span><br><br><br>
            <button class="button-primary" onclick="window.location='https://bit.ly/2HOOWof'">ดูบัญชี</button><br><br>
            <button class="button-primary" onclick="window.location='https://bit.ly/2KBClT1'">ดูรางวัล</button>
            <br><img src="images/survey/bg-ptai-check-ac.jpg" style="max-height:320px;"  class="u-max-full-width" >
            <span style="font-size:x-small; color:#83b200;">WP06</span>
        </div>
        <div class="three columns" style="height:1px;">&nbsp;</div>
        </div>
        </div>
		
		<!--
        <div class="container">
        <div class="row">
        <div class="three columns" style="height:1px;">&nbsp;</div>
        <div class="six columns" style="text-align:center; padding-top:15px;">
            <span style="font-size:1.6em; color:white;" >ขออภัย!</span><br>
            <span style="font-size:1.4em;" >ไม่พบหน้าที่คุณค้นหา</span><br><br><br>
            <button class="button-primary" onclick="window.location='https://bit.ly/2KBClT1'">เข้าสู่เว็บไซต์ Rabbit Rewards</button>
            <br><img src="images/rabbit_nottarget.png" style="max-height:320px;"  class="u-max-full-width" >
            <span style="font-size:x-small; color:#83b200;">WP07</span>
        </div>
        <div class="three columns" style="height:1px;">&nbsp;</div>
        </div>
        </div>
		-->
        <?php
    }else{
        
        $user=new User();
        $res =getUserInfo($email);
        
        
            if($res["meta"]["status"]==1){
				$user=$res["user"];
                

                
                if($user->pointsGiven=='0'){
					//echo"start earn point API";
                    $APIres=sendRewardPoints($email,$xmlString); 
                       if($APIres->response_code==0){
						   //echo"<br> get point!!!";
						

                     
                     $user->pointsGiven=0;
                     $user->PotentialEarn=100;
                     $user->IsMember =  'Y';
                        
                       
                           $result=saveUser($user);
                                if($result["meta"]["status"]!=1){
                                $msgErr=$result["meta"]["msg"];
                                }else{
                                $user=$result["user"];
                                    //echo"<pre>บันทึกสำเร็จ</pre>";
                                    
                                    ?>
        
        <div class="container">
        <div class="row">
        <div class="three columns" style="height:1px;">&nbsp;</div>
        <div class="six columns" style="text-align:center; padding-top:15px;">
            <span style="font-size:1.2em; color:white;" >ยินดีด้วยค่ะ</span><br>
            <img src="images/survey/icn-100p2.png" style="max-height:90px;"><br>
            <span style="font-size:1.6em;" >ได้ถูกเพิ่มบนบัญชีของคุณแล้ว</span><br><br><br>
            <button class="button-primary" onclick="window.location='https://bit.ly/2HOOWof'">ดูบัญชี</button><br><br>
            <button class="button-primary" onclick="window.location='https://bit.ly/2KBClT1'">ดูรางวัล</button>
            <br><img src="images/survey/bg-ptai-check-ac.jpg" style="max-height:320px;"  class="u-max-full-width" >
            <span style="font-size:x-small; color:#83b200;">WP06</span>
        </div>
        <div class="three columns" style="height:1px;">&nbsp;</div>
        </div>
        </div>
                                    
                                    <?php
                                    
                                    
                                }
                    }else{
                        //echo"<pre>ให้ point ไม่ได้</pre>";
						?>
						
						<div class="container">
        <div class="row">
        <div class="three columns" style="height:1px;">&nbsp;</div>
        <div class="six columns" style="text-align:center; padding-top:15px;">
            <span style="font-size:1.6em; color:white;" >ขออภัย!</span><br>
            <span style="font-size:1.4em;" >ดูเหมือนมีบางอย่างผิดพลาด กรุณาลองใหม่ภายหลัง</span><br><br><br>
            <button class="button-primary" onclick="window.location='https://bit.ly/2KBClT1'">เข้าสู่เว็บไซต์ Rabbit Rewards</button>
            <br><img src="images/rabbit_nottarget.png" style="max-height:320px;"  class="u-max-full-width" >
            <span style="font-size:x-small; color:#83b200;">WP010</span>
        </div>
        <div class="three columns" style="height:1px;">&nbsp;</div>
        </div>
        </div>
						
						<?php
                    }
                    
                }else{
					/* API has problem cannot earn point */
                ?>
				 <div class="container">
        <div class="row">
        <div class="three columns" style="height:1px;">&nbsp;</div>
        <div class="six columns" style="text-align:center; padding-top:15px;">
            <span style="font-size:1.6em; color:white;" >ขออภัย!</span><br>
            <span style="font-size:1.4em;" >ดูเหมือนมีบางอย่างผิดพลาด กรุณาลองใหม่ภายหลัง</span><br><br><br>
            <button class="button-primary" onclick="window.location='https://bit.ly/2KBClT1'">เข้าสู่เว็บไซต์ Rabbit Rewards</button>
            <br><img src="images/rabbit_nottarget.png" style="max-height:320px;"  class="u-max-full-width" >
            <span style="font-size:x-small; color:#83b200;">WP010</span>
        </div>
        <div class="three columns" style="height:1px;">&nbsp;</div>
        </div>
        </div>
				
				
				<?php
                }
                
            }else{
                
                $msgErr=$res["meta"]["msg"];

                }
        
        
    }
    
?>

</body>
</html>