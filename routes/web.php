<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('index');
//});

Route::get('/register', function (Request $request) {

    return view('register')->withProfile($request);
});

Route::post('/login', function (Request $request) {
    return view('login')->withProfile($request);
});

Route::get('/thankyou', function () {
    return view('thankyou');
});

//Route::get('/consent', function (Request $request) {
//   $page = Request::get('page');
//   echo $page;
//});

Route::get('/', 'KerryController@index');

Route::post('/update-profile', 'LoginController@loginSSO');


Route::get('/callback', 'FacebookController@callback');

Route::post('/redirect-fb', 'FacebookController@fbRedirect');

Route::post('/registered', 'RegisterController@registerSSO');

Route::post('/verify-otp', 'RegisterController@VerifyOTP');

Route::post('/update-profile-finish', 'ProfileController@UpdatePhoneSSO');

Route::post('/update-otp', 'ProfileController@confirmOTP');

