<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta property="og:image" content="https://rewards.rabbit.co.th/images/favicon/rabbit/mstile-144x144.png"/>
<!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Rabbit Rewards</title>
  <meta name="description" content="Design page by Wiseperzy">
  <meta name="author" content="RabbitRewards Co., Ltd.">



<!-- Favicon -->

<link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/rabbit/xapple-touch-icon-57x57.png.pagespeed.ic.Kv83gfAkcy.webp">
<link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/rabbit/xapple-touch-icon-60x60.png.pagespeed.ic.wPF2y-gMyX.webp">
<link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/rabbit/xapple-touch-icon-72x72.png.pagespeed.ic.3aQDL3Wg7D.webp">
<link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/rabbit/xapple-touch-icon-76x76.png.pagespeed.ic.5i7j7lfwsk.webp">
<link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/rabbit/xapple-touch-icon-114x114.png.pagespeed.ic.lJGztyxuV9.webp">
<link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/rabbit/xapple-touch-icon-120x120.png.pagespeed.ic.x78ZT_IWsx.webp">
<link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/rabbit/xapple-touch-icon-144x144.png.pagespeed.ic.fEKJC-Sl7t.webp">
<link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/rabbit/xapple-touch-icon-152x152.png.pagespeed.ic.AGY-o9Zeb9.webp">
<link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/rabbit/xapple-touch-icon-180x180.png.pagespeed.ic.Cn7F5T_Fxs.webp">
<link rel="icon" type="image/png" href="/images/favicon/rabbit/xfavicon-32x32.png.pagespeed.ic.b_qDo3k2Eq.webp" sizes="32x32">
<link rel="icon" type="image/png" href="/images/favicon/rabbit/xandroid-chrome-192x192.png.pagespeed.ic.JTL1IbgwT-.webp" sizes="192x192">
<link rel="icon" type="image/png" href="/images/favicon/rabbit/xfavicon-96x96.png.pagespeed.ic.g5kxEtDa8A.webp" sizes="96x96">
<link rel="icon" type="image/png" href="/images/favicon/rabbit/xfavicon-16x16.png.pagespeed.ic.VZ99nloZzF.webp" sizes="16x16">
<link rel="manifest" href="/images/favicon/rabbit/manifest.json">

<!-- -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">


<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>   

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>




  <!-- Mobile Specific Metas –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="https://fonts.googleapis.com/css?family=Kanit:300,300i,400,400i,500&amp;subset=thai" rel="stylesheet">

  <!-- CSS –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" type="text/css" href="jquery.datetimepicker.css"/ >
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
 <link rel="stylesheet" href="css/kerry-custom.css">


 
</head>

<body >
<!-- MAIN HEADER-->
<div class="main-header">
    <div class="head-left">
        <img src="images/RR_tagline.png"  onerror="this.onerror=null; this.src='images/RR_tagline.svg'" style="height: 43px;">
    </div>
    <div class="head-right">
        <img src="images/rrlogo.png" onerror="this.onerror=null; this.src='images/rrlogo.svg'" style="height: 40px;">
    </div>  
    <div style="clear:both;"></div> 
</div>


                  

<div class="container">
       
    <div class="row">
        <div class="three columns" style="height:1px;"></div>
        <div class="six columns" style="text-align:center; padding:10px 0px 10px 0px;">
                @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-warning') }}" style="font-weight: 400;font-size:18px;">{{ Session::get('message') }}</p>

                @endif
            <br>
            <span style="color:#ff8300; font-size:1.8em; line-height: 1.2em;">
               เข้าสู่ระบบ<br><br>
                <form action="{{url('/redirect-fb')}}" method="post" id="optionForm" class="form-group" >
                     @csrf
                        <input type="hidden" name="first_name" value="{{$profile['first_name']}}">
                        <input type="hidden" name="last_name" value="{{$profile['last_name']}}">
                        <input type="hidden" name="gender" value="{{$profile['gender']}}">
                        <input type="hidden" name="phone" value="{{$profile['phone']}}">
                 <button onclick="click_button()" id="button-d" type="submit" class="btn-primary loginBtn--facebook u-full-width" style="padding-bottom:12px;margin-bottom:12px;" ><span style="margin:3px 6px 5px 4px;padding-bottom:3px;border-right: 1px solid rgba(215, 202, 202, 0.37);"><img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_facebook.png" style="vertical-align: sub;" max-height="14" max-width="14"> </span>&nbsp;ล๊อคอินด้วย Facebook</button><br>
                  <a  style="color:#333333; text-align:left; font-size:15px;">หรือ</a>
               </form>
              
            </span>
           
        </div>
        <div class="three columns" style="height:1px;"></div>
    </div>
    <form action="{{url('/update-profile')}}" method="post" id="optionForm" class="form-group" >
         @csrf
         <input type="hidden" name="first_name" value="{{$profile['first_name']}}">
        <input type="hidden" name="last_name" value="{{$profile['last_name']}}">
        <input type="hidden" name="gender" value="{{$profile['gender']}}">
        <input type="hidden" name="phone" value="{{$profile['phone']}}">
    <div class="row">
       <div class="three columns" style="height:1px;"></div>
        <div class="six columns" style="text-align:center; padding:10px 2% 10px 2%;">
            <input type="email" name="email" class="u-full-width" placeholder="อีเมล" required style="text-align:center;"><br>
            <input type="password" name="password" class="u-full-width" placeholder="รหัสผ่าน"pattern=".{4,10}" maxlength="10" data-fv-stringlength-max="10" required style="text-align:center;"><br>
          
            <button type="submit"  class="button-primary u-full-width" >เข้าสู่ระบบ</button>
        </div>
       <div class="three columns" style="height:1px;"></div>
    </div>
    </form>
    
    
</div>                 



</body>
</html>