<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
class ProfileController extends Controller
{
//     public function show($email)
//     {
//         $profile = self::GetCurrentProfile($email);
//         return view('update')->withProfile($profile);
//     }
//    
    
       public function UpdatePhoneSSO(Request $request){
        $updatePhone = new Client();


        $res_phone = $updatePhone->patch('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards-campaign/users/by-email/'.$request->email,    //staging
//        $res_phone = $updatePhone->patch('https://sso.rabbit.co.th/v1/sso-user/rewards-campaign/users/by-email/'.$request->email,    //production
                            [
                                'form_params' =>  [
                                    
                                    'first_name' => $request->first_name,
                                    'last_name' => $request->last_name,
                                    'title' => $request->title,
                                    'gender' => $request->gender,
                                    'email' => $request->email,
                                    'phone' => $request->phone
                                    
                                   ],
                            ]);

        $code = $res_phone->getStatusCode();
        
        if($request->phone == $request->oldphone){
             return view('thankyou');
        }else{
            return view('otp-update')->withPhone($request->phone);
        }
        
        
    }

    public function confirmOTP(Request $request){

        /*********** Check Verification email and save to SSO ********/
        $verifyOTP = new Client();
//        $token = 'mGgtDgdEX4q48cKBk9X5DvPeS4mW8xsz';    //staging token
         $token = 'BuY4sftGPxJZ6ymCka7fvbYUN5aMZsFd';    //production token
        try {
              /***** stagging ******/
             $res = $verifyOTP->post('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards-campaign/activation/phone',
              /***** production *****/
//               $res = $verifyOTP->post('https://sso.rabbit.co.th/v1/sso-user/rewards-campaign/activation/phone',
                             [
                                 'headers' => [
                                   'Authorization' => 'Bearer ' . $token ],
                                 'form_params' =>  [
                                   'phone' => $request->phone,
                                   'code' => $request->code,
                                 ],
                             ]);
                
              
                return view('thankyou');

        }catch (\Exception $e){
                Session::flash('message', 'รหัสไม่ถูกต้อง กรุณาตรวจสอบใหม่อีกครั้ง');
                return view('otp-update')->withPhone($request->phone);
            }
  
    }
    
   
    

    
//	public function ResendOTP(Request $request){
//        $updatePhone = new Client();
//        /***** Update 5 data except email to SSO *****/
//
// //       $res_phone = $updatePhone->patch('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards-campaign/users/by-email/'.$profile->pre_email,    //staging
//        $res_phone = $updatePhone->patch('https://sso.rabbit.co.th/v1/sso-user/rewards-campaign/users/by-email/'.$request->email,    //production
//                            [
//                                'form_params' =>  [
//                                    'phone' => $request->phone
//                                   ],
//                            ]);
//
//        $code = $res_phone->getStatusCode();
//        return $code;
//	}
    
    
}
