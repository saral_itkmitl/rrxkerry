<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

use Socialite;
use Session;

class FacebookController extends Controller
{
    
   public function fbRedirect(Request $request)
            
    {
            try {
                 Session::put('first_name', $request->first_name);
                Session::put('last_name', $request->last_name);
                Session::put('phone', $request->phone);
                Session::put('gender', $request->gender);
                return Socialite::driver('facebook')->redirect();
            }catch (\Exception $e){
                Session::flash('message', 'กรุณาลองใหม่อีกครั้ง');
                return Redirect::to('/');
            }
    }
    
    
        public function callback()
    {
//        try{ 
            
            $user  = Socialite::driver('facebook')->user();
            $token = $user->token;
            
            $first_name = Session::get('first_name');
            $last_name = Session::get('last_name');
            $phone = Session::get('phone');
            $gender = Session::get('gender');
            
            $data = [];
            $data[] = [
                
                'first_name' => $first_name,
                'last_name' => $last_name,
                'phone' => $phone,
                'gender' => $gender,
                'email' => $user->user['email']
            ];

            $profile = $data[0];
            if(empty($user->user['email'])){
                Session::flash('message', 'กรุณาอนุญาตการเข้าถึงอีเมลของคุณทาง Facebook');
                return view('login')->withProfile($profile);
            }
            $first_name = Session::forget('first_name');
            $last_name = Session::forget('last_name');
            $phone = Session::forget('phone');
            $gender = Session::forget('gender');
            

            if(self::CheckFacebookToken($token)){
                    
                $current   = self::GetCurrentProfile($user->user['email']);
                
                if(self::UpdatePhoneSSO($profile)){
                    if($profile['phone'] == $current['phone']){
                        return view('thankyou');
                    }else{
                        return view('otp-update')->withPhone($profile['phone']);
                    }
                }else{
                    Session::flash('message', 'เกิดข้อผิดพลาดระหว่างเชื่อมต่อ กรุณาลองใหม่อีกครั้ง');
                    return view('login')->withProfile($profile);
                }
			}else{
                    Session::flash('message', 'คุณยังไม่ได้เป็นสมาชิก');
                    return view('login')->withProfile($profile);
                
            }
            
       
            
            
            
//         }catch (\Exception $e){  
              
             
              return back();
            
//          }   
    }
    
    
        public function CheckFacebookToken($facebookToken){
           try {  
//               return $token;
                 $token = 'qZqu0EK8aL09xTiirvSyC0cKr59jVCId';    //staging token
//        $token = '5qfeQ2kqHmQT5CyzgQYpY8czNMU8HX6Z';    //production token
                  $fbToken = new Client();
                  $res_fb = $fbToken->post('https://staging-sso-sso.rabbitinternet.com/v1/sso-auth/rewards/login/fb',    //staging
//        $res_fb = $fbToken->post('https://sso.rabbit.co.th/v1/sso-auth/rewards/login/fb',    //production
                            [   
                                'headers' => [
                                   'Authorization' => 'Bearer ' . $token ],
                                'form_params' =>  [
                                    'fb_token' => $facebookToken,
                                ],
                            ]);
              
//              $data   = $res_fb->getBody()->getContents();
//              $json = json_decode($data,true);
//               dd($data);
               
//              return $json['data']['sid'];
              return true;
           }catch (\Exception $e){  
              return false;
              
          } 
        }
    
  

    public function GetCurrentProfile($email){
        try {

        $getProfile = new Client();
//        $token = 'qZqu0EK8aL09xTiirvSyC0cKr59jVCId';    //staging token
        $token = '5qfeQ2kqHmQT5CyzgQYpY8czNMU8HX6Z';    //production token
        /***** stagging ******/
        $res = $getProfile->get('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards/users/email/'.$email,
        /***** production *****/
//        $res = $getProfile->get('https://sso.rabbit.co.th/v1/sso-user/rewards/users/email/'.$email,
                       [
                           'headers' => [
                           'Authorization' => 'Bearer ' . $token ]
                       ]);

        $data = $res->getBody()->getContents();

        $json = json_decode($data,true);

            return $json['data'];
        }catch (\Exception $e) {
            return false;
        }
    }

    public function UpdatePhoneSSO($kerry){
        try {
            $updatePhone = new Client();
            if($kerry['gender'] == 'M'){
                $gender = 'male';
            }else if($kerry['gender'] == 'F'){
                $gender = 'female';
            }else{
                $gender = 'other';
            }  

        $res_phone = $updatePhone->patch('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards-campaign/users/by-email/'.$kerry['email'],    //staging
//        $res_phone = $updatePhone->patch('https://sso.rabbit.co.th/v1/sso-user/rewards-campaign/users/by-email/'.$request->email,    //production
                            [
                                  'form_params' =>  [
                                    
                                    'first_name' => $kerry['first_name'],
                                    'last_name' => $kerry['last_name'],
                                    'gender' => $gender,
                                    'email' => $kerry['email'],
                                    'phone' => $kerry['phone']
                                    
                                   ],
                            ]);

        $code = $res_phone->getStatusCode();
                    return true;
          }catch (\Exception $e) {
                    return false;
          }
    }
    
    public function confirmOTP(Request $request){

        /*********** Check Verification email and save to SSO ********/
        $verifyOTP = new Client();
//        $token = 'mGgtDgdEX4q48cKBk9X5DvPeS4mW8xsz';    //staging token
         $token = 'BuY4sftGPxJZ6ymCka7fvbYUN5aMZsFd';    //production token
        try {
              /***** stagging ******/
             $res = $verifyOTP->post('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards-campaign/activation/phone',
              /***** production *****/
//               $res = $verifyOTP->post('https://sso.rabbit.co.th/v1/sso-user/rewards-campaign/activation/phone',
                             [
                                 'headers' => [
                                   'Authorization' => 'Bearer ' . $token ],
                                 'form_params' =>  [
                                   'phone' => $request->phone,
                                   'code' => $request->code,
                                 ],
                             ]);
                
              
                return view('thankyou');

        }catch (\Exception $e){
                return view('otp-update')->withPhone($request->phone);
            }
  
    }
    
}
