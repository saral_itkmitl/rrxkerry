<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use Request;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Contracts\Encryption\DecryptException;

class KerryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $UUID = Request::get('UUID');
//        $cookie = Cookie::get('rrxkerry_session');
        $cookie = Cookie::get('sso_session_id');
//        $value = request()->cookie();
//        echo $_COOKIE["sso_session_id"];

        
//        $result = decrypt($cookie);

//        dd ($decrypted);
        $token = self::getKey();
       
        
        if($token){
            
//            dd ($token);
            $profile = self::getProfile($UUID,$token);
            
            if($profile){
//                dd ($profile);
                if(empty($profile["surname"])){
                
                    $profile["surname"] = 'กรุณาระบุ';

                }
                if(!empty($cookie)){
                    $decrypted = self::customDecrypt($cookie);
                    $userid = self::getUsersession($decrypted);
                    $profile_loggedin = self::getUserbyid($userid);
//                     dd ($profile_loggedin);
                     if($profile_loggedin){

                        if(self::UpdatePhoneSSO($profile_loggedin['email'], $profile)){
                            
                                if($profile_loggedin['phone'] == $profile['mobile']){
                                    return view('thankyou');
                                }else{
                                    return view('otp-update')->withPhone($profile['mobile']);
                                }
                            
                        }else{

                                return  view('index')->withProfile($profile);
                        }
                     }else{

                            return  view('index')->withProfile($profile);
                     }

                   
                }else{
                    return view('index')->withProfile($profile);
                }
                
                
                
  
            }else{
                
                return redirect('https://rewards.rabbit.co.th/');
                
            }
            
//             dd ($profile);
        }else{
            
            return redirect('https://rewards.rabbit.co.th/');
            
        }
//        dd ($token);

    }
    
    public function UpdatePhoneSSO($ssoemail, $kerry){
    try {
                $updatePhone = new Client();
            if($kerry->gender == 'M'){
                $gender = 'male';
            }else if($kerry->gender == 'F'){
                $gender = 'female';
            }else{
                $gender = 'other';
            }  

        $res_phone = $updatePhone->patch('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards-campaign/users/by-email/'.$ssoemail,    //staging
//        $res_phone = $updatePhone->patch('https://sso.rabbit.co.th/v1/sso-user/rewards-campaign/users/by-email/'.$ssoemail,    //production
                            [
                                'form_params' =>  [
                                    
                                    'first_name' => $kerry['first_name'],
                                    'last_name' => $kerry['last_name'],
                                    'title' => $kerry['title'],
                                    'gender' => $gender,
                                    'email' => $kerry['email'],
                                    'phone' => $kerry['mobile']
                                    
                                   ],
                            ]);

        $code = $res_phone->getStatusCode();
                    return true;
          }catch (\Exception $e) {
                    return false;
          }
    }
    
    public function customDecrypt($vWord){
        $customKey = "HL2E9qdXrWvW+XwM4P5PdapatOLM31nZx9NQ8M8WoUg=";
        $newEncrypter = new \Illuminate\Encryption\Encrypter( base64_decode($customKey), 'AES-256-CBC');
        return $newEncrypter->decrypt( $vWord );
    }

     public function getUsersession($sid)
    {
        
        try{
            
            $client    = new Client();
            $token  = 'qZqu0EK8aL09xTiirvSyC0cKr59jVCId'; //stg
//            $token  = '5qfeQ2kqHmQT5CyzgQYpY8czNMU8HX6Z';
            $res       = $client->get('https://staging-sso-sso.rabbitinternet.com/v1/sso-auth/rewards/session/'.$sid,    //staging
//              $res       = $client->get('https://sso.rabbit.co.th/v1/sso-auth/rewards/session/'.$sid,    //production
                [
                    'headers' => [
                        //                                    'Content-Type' => 'application/json', //  it's got error 422
                        'Authorization' => 'Bearer ' . $token
                ]
                    
//                      'body' => json_encode([
//                        'UUID' => ,
//                   
//                    ]),
//                    'debug' => true
                ]);
        
            $body = $res->getBody()->getContents();
        
            $code = $res->getStatusCode();
        
            $data = json_decode($body,true);
//            dd ($data);
            return $data['data']['user_id'];
            
        }catch(\Exception $e){
             return false;
        }
    }
    
       public function getUserbyid($userid)
    {
        
        try{
            
            $client    = new Client();
            $token  = 'qZqu0EK8aL09xTiirvSyC0cKr59jVCId'; //stg
//            $token  = '5qfeQ2kqHmQT5CyzgQYpY8czNMU8HX6Z';
            $res       = $client->get('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards/users/id/'.$userid,    //staging
//          $res       = $client->get('https://sso.rabbit.co.th/v1/sso-user/rewards/users/id/'.$userid,    //production
                [
                    'headers' => [
                        //                                    'Content-Type' => 'application/json', //  it's got error 422
                        'Authorization' => 'Bearer ' . $token
                ]
                    
//                      'body' => json_encode([
//                        'UUID' => ,
//                   
//                    ]),
//                    'debug' => true
                ]);
        
            $body = $res->getBody()->getContents();
        
            $code = $res->getStatusCode();
        
            $data = json_decode($body,true);
//            dd ($data);
            return $data;
            
        }catch(\Exception $e){
             return false;
        }
    }
    
    public function getKey()
    {
        try{
            
            $email = 'rabbit@bssh.com';
            $password = 'zr*3H$C@Y+2vwAR5';
//            
//            $email = 'saralk@rabbit.co.th';
//            $password = 'saral012';
            
            $client    = new Client();
        
            $res       = $client->post('http://stg-api.rabbit.co.th/v2/admin/user/login',  //staging
//            $res       = $client->post('https://bssh.rabbit.co.th/v2/admin/user/login',    //production
                [
                    
                    'body' => json_encode([
                        'email' => $email,
                        'password' => $password
                ]),


//                    'debug' => true
                ]);
            
            $body = $res->getBody()->getContents();
        
            $code = $res->getStatusCode();
        
            $data = json_decode($body,true);
        
            
            $accesstoken = $data['result'][0]['accesstoken'];

            return $accesstoken;
            
        }catch(\Exception $e){
            
            return false;
        }
    }

  
    public function getProfile($UUID,$accesstoken)
    {
        
        try{
            
            $client    = new Client();
            
            $res       = $client->get('http://stg-api.rabbit.co.th/v2/kerry/recipient/'.$UUID,    //staging
//            $res       = $client->get('https://bssh.rabbit.co.th/v2/kerry/recipient/'.$UUID,    //production
                [
                    'headers' => [
                        //                                    'Content-Type' => 'application/json', //  it's got error 422
                        'Authorization' => 'Bearer ' . $accesstoken
                ]
                    
//                      'body' => json_encode([
//                        'UUID' => ,
//                   
//                    ]),
//                    'debug' => true
                ]);
        
            $body = $res->getBody()->getContents();
        
            $code = $res->getStatusCode();
        
            $data = json_decode($body,true);
//            dd ($data);
            return $data['result'];
            
        }catch(\Exception $e){
             return false;
        }
    }

 
}
