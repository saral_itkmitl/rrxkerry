<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Session;
class LoginController extends Controller
{
    public function loginSSO(Request $request)
    {
        
//        try {
            $email = $request->email;   //Get email form login page
            $password = $request->password;   //Get password form login page
            $data = [];
            $data[] = [
                
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'phone' => $request->phone,
                'gender' => $request->gender,
                'email' => $request->email
            ];

            $profile = $data[0];
             if(self::logintoSSO($request->email, $request->password)){

            $current   = self::GetCurrentProfile($request->email);          
            if(self::UpdatePhoneSSO($request)){
                if($request->phone == $current['phone']){
                    return view('thankyou');
                }else{
                    return view('otp-update')->withPhone($request->phone);
                }
            }else{
                Session::flash('message', 'เซิฟเวอร์ไม่ตอบสนอง กรุณาลองใหม่อีกครั้ง');
                return  view('login')->withProfile($profile);
            }

            return  view('update')->withProfile($current)->withNewphone($request->phone);

            }else{
                    Session::flash('message', 'อีเมลหรือรหัสผ่านไม่ถูกต้อง');
            return  view('login')->withProfile($request);
             }

    }
      public function logintoSSO($email,$password){
           try {
          
             /*** Do login  **/
            $client    = new Client();
            
            $token = 'qZqu0EK8aL09xTiirvSyC0cKr59jVCId';   //staging
//      			$token     = '5qfeQ2kqHmQT5CyzgQYpY8czNMU8HX6Z'; //production
            $res       = $client->post('https://staging-sso-sso.rabbitinternet.com/v1/sso-auth/rewards/login',    //staging
//            $res       = $client->post('https://sso.rabbit.co.th/v1/sso-auth/rewards/login',    //production
                [
                    'headers' => [
                        //                                    'Content-Type' => 'application/json', //  it's got error 422
                        'Authorization' => 'Bearer ' . $token
                                 ],
                    'form_params' => [
                        'credential' => $email,
                        'password' => $password],
                ]);
               
            $body = $res->getBody()->getContents();
        
            $code = $res->getStatusCode();
        
            $data = json_decode($body,true);

            return $data;
               
          }catch (\Exception $e) {
               return false;
          }
      }
    
    
      public function UpdatePhoneSSO($kerry){
                try {
        $updatePhone = new Client();
        if($kerry->gender == 'M'){
                $gender = 'male';
            }else if($kerry->gender == 'F'){
                $gender = 'female';
            }else{
                $gender = 'other';
            }  

        $res_phone = $updatePhone->patch('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards-campaign/users/by-email/'.$kerry->email,    //staging
//        $res_phone = $updatePhone->patch('https://sso.rabbit.co.th/v1/sso-user/rewards-campaign/users/by-email/'.$request->email,    //production
                            [
                                'form_params' =>  [
                                    
                                    'first_name' => $kerry->first_name,
                                    'last_name' => $kerry->last_name,
                                    'title' => $kerry->title,
                                    'gender' => $gender,
                                    'email' => $kerry->email,
                                    'phone' => $kerry->phone
                                    
                                   ],
                            ]);

        $code = $res_phone->getStatusCode();
                    return true;
          }catch (\Exception $e) {
                    return false;
          }
    }
    
       public function GetCurrentProfile($email){
          try {
            $getProfile = new Client();
    //        $token = 'qZqu0EK8aL09xTiirvSyC0cKr59jVCId';    //staging token
            $token = '5qfeQ2kqHmQT5CyzgQYpY8czNMU8HX6Z';    //production token
            /***** stagging ******/
            $res = $getProfile->get('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards/users/email/'.$email,
            /***** production *****/
    //        $res = $getProfile->get('https://sso.rabbit.co.th/v1/sso-user/rewards/users/email/'.$email,
                           [
                               'headers' => [
                               'Authorization' => 'Bearer ' . $token ]
                           ]);

            $data = $res->getBody()->getContents();

            $json = json_decode($data,true);
              
               return $json['data'];
         }catch (\Exception $e) {
               return false;
           }    
       
    }
}
