<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Session;
class RegisterController extends Controller
{
     public function registerSSO(Request $request)
    {	
        try{
           
            $first_name ='kerry';
            $last_name = 'recipient';
            if($request->gender == 'M'){
                $gender = 'male';
            }else if($request->gender == 'F'){
                $gender = 'female';
            }else{
                $gender = 'other';
            }
            
            /***** register to API *****/
            $registerInfo = new Client();
            $res_backoffice = $registerInfo->post('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards-campaign/register',    //staging
//             $res_backoffice = $registerInfo->post('https://sso.rabbit.co.th/v1/sso-user/rewards-campaign/register',    //production
                [
                    'form_params' =>  [
                        'registration_type' => 'kerry_recipient_sms',
                        'first_name' => $request->first_name,
                        'last_name' => $request->last_name,
                        'phone' => $request->phone,
                        'language' => 'th',
                        'gender' => $gender,
                        'email' => $request->email,
                        'password' => $request->password,
                        'is_tc_accepted' => true,
                    ],
                ]);

//            dd ($profile);
            return  view('otp')->withPhone($request->phone);
        }catch (\Exception $e){
            $profile['name'] = $request->first_name;
            $profile['surname'] = $request->last_name;
            $profile['mobile'] = $request->phone;
            $profile['gender'] = $request->gender;
            Session::flash('message', 'อีเมลนี้ถูกใช้ไปแล้ว');

            return view('index')->withProfile($profile);
            }
    }
    


    
     public function VerifyOTP(Request $request){

        /*********** Check Verification email and save to SSO ********/
        $verifyOTP = new Client();
        $token = 'mGgtDgdEX4q48cKBk9X5DvPeS4mW8xsz';    //staging token
//         $token = 'BuY4sftGPxJZ6ymCka7fvbYUN5aMZsFd';    //production token
        try {
              /***** stagging ******/
             $res = $verifyOTP->post('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards-campaign/activation/phone',
              /***** production *****/
//               $res = $verifyOTP->post('https://sso.rabbit.co.th/v1/sso-user/rewards-campaign/activation/phone',
                             [
                                 'headers' => [
                                   'Authorization' => 'Bearer ' . $token ],
                                 'form_params' =>  [
                                   'phone' => $request->phone,
                                   'code' => $request->code,
                                 ],
                             ]);
                
       
                return view('thankyou');

        }catch (\Exception $e){
                Session::flash('message', 'ไม่ถูกต้อง กรุณาตรวจสอบรหัสที่ระบบส่งไปยัง SMS ของคุณอีกครั้ง');
            return view('otp')->with('phone', $request->phone);
            }
  
    }


}
